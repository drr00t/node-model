var model = require('model');

//var adapter = model.createAdapter('postgres', {
//	  host: 'localhost',
//	  username: 'user',
//	  password: 'password',
//	  dbname: 'mydb'
//	});

model.defaultAdapter = model.createAdapter('sqlite');

var Gateway = function () {
	  this.property('mac', 'string', {required: true});
	  this.property('ip', 'string', {required: true});
//	  this.validatesPresent('mac');
	  this.hasMany('Sensor');
//	  this.validatesFormat('mac', /[a-z]+/, {message: 'Subdivisions!'});
//	  this.validatesLength('login', {min: 3});
//	  this.validatesConfirmed('password', 'confirmPassword');
//	  this.validatesWithFunction('password', function (s) {
//	      // Something that returns true or false
//	      return s.length > 0;
//	  });
};

Gateway = model.register('Gateway', Gateway);

var Sensor = function () {
	  this.property('index', 'int', {required: true});
	  this.property('tag', 'string', {required: true});
	  this.belongsTo('Gateway');

//	  this.validatesFormat('mac', /[a-z]+/, {message: 'Subdivisions!'});
//	  this.validatesLength('login', {min: 3});
//	  this.validatesConfirmed('password', 'confirmPassword');
//	  this.validatesWithFunction('password', function (s) {
//	      // Something that returns true or false
//	      return s.length > 0;
//	  });
};
Sensor = model.register('Sensor', Sensor);

var gw = Gateway.create({mac:'01:20:40:30:49:39',ip:'123.123.123.12'});

gw.save(function(err,data){
	if(err)
		console.log(err);
	
	gw.addSensor(Sensor.create({index:1,tag:'temp'}));
	gw.addSensor(Sensor.create({index:2,tag:'temp'}));
	gw.addSensor(Sensor.create({index:3,tag:'temp'}));
	
	gw.save(function(err,data){
		if(err)
			console.log(err);
		
		console.log(JSON.stringify(data));
		
		gw.getSensors(function(err,data){
			console.log(JSON.stringify(data));
		});
	});
});

//Gateway.create({mac:'01:20:40:30:43:39',ip:'12.12.12.12'}).save(function(err,data){
//	if(err)
//		console.log(err);
//	data.addSensor(Sensor.create({index:1,tag:'temp'}));
//	data.addSensor(Sensor.create({index:2,tag:'temp'}));
//	data.addSensor(Sensor.create({index:3,tag:'temp'}));
//});
//Gateway.create({mac:'01:20:40:30:43:39',ip:'12.12.12.12'}).save(function(err,data){
//	if(err)
//		console.log(err);
//	
//	data.addSensor(Sensor.create({index:1,tag:'parking'}));
//	data.addSensor(Sensor.create({index:2,tag:'parking'}));
//	data.addSensor(Sensor.create({index:3,tag:'parking'}));
//});
//Gateway.create({mac:'01:20:40:30:43:39',ip:'12.12.12.12'}).save(function(err,data){
//	if(err)
//		console.log(err);
//	data.addSensor(Sensor.create({index:1,tag:'water'}));
//	data.addSensor(Sensor.create({index:2,tag:'water'}));
//	data.addSensor(Sensor.create({index:3,tag:'water'}));
//});
//Gateway.create({mac:'01:20:40:30:43:39',ip:'12.12.12.12'}).save(function(err,data){
//	if(err)
//		console.log(err);
//	data.addSensor(Sensor.create({index:1,tag:'air'}));
//	data.addSensor(Sensor.create({index:2,tag:'air'}));
//	data.addSensor(Sensor.create({index:3,tag:'air'}));
//});


//console.log(JSON.stringify(gateway));
console.log('gravados com sucesso\n\n\n');

Gateway.first({ip:'123.123.123.12'},function(err,data){
	if(err)
		console.log(err);
	
	console.log(JSON.stringify(data));	
});

